# Stage 1: Build the Angular app
FROM node:16 as build

WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install Node.js dependencies, including @angular/cli
RUN npm install -g @angular/cli@latest
RUN npm install

# Copy the rest of the app source code
COPY . .

# Build the Angular app with production configuration
RUN ng build --configuration=production

# Stage 2: Create the production image
FROM nginx:alpine

# Copy the built Angular app from Stage 1 to the Nginx public directory
COPY --from=build /usr/src/app/dist /usr/share/nginx/html

# Expose port 80 for serving the app
EXPOSE 80

# Start Nginx with the 'daemon off;' option to keep it running in the foreground
CMD ["nginx", "-g", "daemon off;"]
